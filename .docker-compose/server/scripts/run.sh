#!/bin/bash

## Server Start Up
#
printf "\033[0;32m > Server Running ...\n"

## Boot Server
#
if [ ! -f /app/runtime/initialized ]; then
   bootup
fi

tail -f /dev/null