#!/bin/bash

printf "\033[0;32m > Bootup ...\n\n"

if [ -f /app/runtime/initialized ]; then
   rm /app/runtime/initialized
fi

chmod 0777 -R /app/runtime

printf "\033[0;32m > Create tables structure ...\n\n"
(cd /app/ && ./otrium create-tables)

printf "\033[0;32m > Importing sample data for the first run ...\n"
printf "\033[0;32m Manual Import can be done by command: otrium seed-samples \n\n"
(cd /app/ && ./otrium seed-samples)

touch /app/runtime/initialized
