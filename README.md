Otrium Challenge
=========

[![Build Status](https://travis-ci.com/InterviewAssignments/otrium.svg?token=yWDvszvqyzqkuCEvCd16&branch=master)](https://travis-ci.com/InterviewAssignments/otrium)

### Task

Attached you’ll find a [SQL file](./data/otrium_challenge.1611679659.sql) with two tables containing dummy data:

- **Brands:** containing data of fictive brands.
- **GMV:** containing the total turnover per day for all brands.
  The challenge is to create a script that will generate a table inside a CSV file with at least the following data:
- Turnover per brand (all brands)
- Turnover per day (of all brands)
- Scope: data should only contain the first 7 days of the month May (01-05-2018 - 07-05-2018).
  **End result:** the end result should be a single table (inside a single CSV file) containing the aforementioned data. CSV
  needs to be easy to read/analyse for a person on a daily basis.
  

  **Bonus:** the prices in the database are 21% VAT included. Bonus task is:
- Show the excluded VAT per brand as well (21%)

# Considerations

[ ] Tests are not fully covered which I'm aware of, due to time limitation I decided to write test for main functionality
    and most complicated component.

As a sample, one of my prior developed PHPUnit tests can be found [here](https://gitlab.com/poirot/std/-/tree/master/test), written for Standard packages of Poirot Framework.

## Dependencies

For Console  Implementation:
* [symfony/console](https://symfony.com/doc/current/components/console.html)  - Create command-line commands.

General porpuse components (private packages from my own framework):
* [poirot/std](https://poirot-framework.gitbook.io/poirot/components/standard-core-libraries)  - Sets of useful basic libraries and helpers.
* [poirot/servicemanager](https://poirot-framework.gitbook.io/poirot/components/ioc)  - Easy-to-use implementation of the Service Locator design pattern.

## How to run

With installed [Docker](https://www.docker.com/get-started) and [docker-compose](https://docs.docker.com/compose/install/) it's easy to run the application as it's fully dockerized and don't need any configuration.


1. From project root
2. Run `cp .env.dist .env` and change environment variables for your need.
3. Build app image with `docker-compose build`

To make work with application easier you can use docker-compose:
```bash
docker-compose up -d # start application
docker-compose down # stop and remove containers
docker-compose exec app bash # connect to container command line
docker-compose exec app ./otrium # execute application
```


**To Export turnovers CSV File using docker-compose:**

![Exported CSV File](./data/turnovers-export.png)
_file can be found in `./app/runtime` directory after executing script._

```bash
docker-compose up -d # start application
docker-compose exec app ./otrium export-csv
```

**Note:** In any case if tables not created by default on first initialization by docker, run these commands to create them manually:
first run into container: `docker-compose exec app /bin/bash` Then:

To Create Table Scheme
```
./otrium create-tables
```

To Feed and Import Sample Data:
```
./otrium feed-samples
```

## Tests

PHPUnit Tests.

Travis-CI Integration.

To run tests cases on your local machine with installed docker-compose, run:

```bash
docker-compose exec app ./vendor/bin/phpunit
```
