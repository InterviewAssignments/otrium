<?php
declare(strict_types=1);

use Otrium\Console\Commands\CreateTablesCommand;
use Otrium\Console\Commands\ExportTurnovers;
use Otrium\Console\Commands\SeedSamplesCommand;
use Otrium\Services\Console\Builder;

/**
 * Registered Console Application`s Commands and Configurations,
 * Settings should be valid to @see Builder.
 *
 */
return [
    'commands' => [
        new CreateTablesCommand,
        new SeedSamplesCommand,
        new ExportTurnovers,
    ],
    // Declaring Application As a Single Command App. With The Only and One Default Handler.
    # 'default_command' => 'command_name',
];
