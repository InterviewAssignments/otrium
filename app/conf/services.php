<?php
declare(strict_types=1);

use Otrium\Interfaces\Entity\iBrandFactory;
use Otrium\Interfaces\iApplication;
use Otrium\Interfaces\iTabularDataWriter;
use Otrium\Interfaces\Repository\iTurnoversRepo;
use Otrium\Libraries\TabularDataFileWriter;
use Otrium\Models\Entities\Brands\BrandFactory;
use Otrium\Models\Repositories\TurnoversRepo;
use Otrium\Providers\ApplicationProvider;
use Otrium\Providers\PdoProvider;
use Poirot\ServiceManager\Services\Factory;
use Poirot\ServiceManager\Services\Instance;
use Poirot\ServiceManager\Services\Value;


return [
    'implementations' => [
        'app' => iApplication::class,
        'db' => \PDO::class,
    ],
    'services' => [
        'app' => new ApplicationProvider,
        'db' => (new PdoProvider)
            ->setHost(getenv('DB_HOST'))
            ->setDbname(getenv('DB_DBNAME'))
            ->setUsername(getenv('DB_USERNAME'))
            ->setPassword(getenv('DB_PASSWORD')),

        // Repositories
        iTurnoversRepo::class => new Instance(TurnoversRepo::class),
        iBrandFactory::class => new Value(new BrandFactory),

        // Helpers
        iTabularDataWriter::class => new Factory(function() {
            return new TabularDataFileWriter(DIR_RUNTIME . DS . 'turnover.csv');
        })
    ],
];
