<?php
declare(strict_types=1);

namespace Otrium\Providers;

use Poirot\ServiceManager\{
    Interfaces\iServicesContainer,
    Interfaces\Pacts\iServicesAware,
    Services\aService,
};

use Otrium\Interfaces\iApplication;
use Otrium\Services\Console\Builder as ConsoleBuilder;
use Otrium\Services\ConsoleSapi;


class ApplicationProvider
    extends aService
    implements iServicesAware
{
    private iServicesContainer $serviceManager;

    /**
     * @inheritdoc
     *
     * @return iApplication
     */
    function createService(): iApplication
    {
        if (! $this->isCommandLineSapi()) {
            throw new \RuntimeException(
                'This application is intended as a Command Line and cant be run on Http Sapi.'
            );
        }

        $consoleApp = new ConsoleSapi($this->serviceManager);

        (new ConsoleBuilder(
            ConsoleBuilder::from(DIR_CONFIG . DS . 'console.php')
        ))->build($consoleApp);

        return $consoleApp;
    }

    // Implement Service Manager Aware Interface

    function setServicesContainer(iServicesContainer $sm): void
    {
        $this->serviceManager = $sm;
    }

    // ..

    private function isCommandLineSapi(): bool
    {
        return 'cli' === php_sapi_name();
    }
}
