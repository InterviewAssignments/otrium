<?php
declare(strict_types=1);

namespace Otrium\Providers;

use Poirot\ServiceManager\Services\aService;


class PdoProvider
    extends aService
{
    protected string $host;
    protected string $dbname;
    protected string $username;
    protected ?string $password;

    /**
     * @inheritdoc
     */
    function createService()
    {
        $conn =  new \PDO(
            sprintf('mysql:host=%s;dbname=%s', $this->host, $this->dbname),
            $this->username,
            $this->password
        );

        // set the PDO error mode to exception
        $conn->setAttribute(\PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'utf8'");
        $conn->setAttribute(\PDO::MYSQL_ATTR_INIT_COMMAND, "SET CHARACTER SET 'utf8'");
        $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $conn->exec('SET NAMES \'utf8\'');
        $conn->exec('SET CHARACTER SET \'utf8\'');

        return $conn;

    }

    // Settings:

    function setHost(string $host): self
    {
        $this->host = $host;
        return $this;
    }

    function setDbname(string $dbname): self
    {
        $this->dbname = $dbname;
        return $this;
    }

    function setUsername(string $username): self
    {
        $this->username = $username;
        return $this;
    }

    function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }
}
