<?php
declare(strict_types=1);

namespace Otrium\Console\Commands;

use Otrium\Console\BaseCommand;
use Otrium\Console\Commands\CreateTables\CreateTables;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class CreateTablesCommand
    extends BaseCommand
{
    /** @var string FQCN (classFQN) to invokable command action */
    protected string $commandInvokableClass = CreateTables::class;

    /** @var OutputInterface */
    private $output;

    function configure(): void
    {
        $this->setName('create-tables')
             ->setDescription('Create database tables.');
    }

    /**
     * @inheritDoc
     */
    function getExecutableArguments(): array
    {
        return [
            'output' => $this->output,
        ];
    }

    /**
     * @inheritDoc
     */
    protected function beforeExecute(InputInterface $input, OutputInterface $output): void
    {
        $this->output = $output;
    }
}
