<?php
declare(strict_types=1);

namespace Otrium\Console\Commands;

use Otrium\Console\BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class ExportTurnovers
    extends BaseCommand
{
    /** @var string FQCN (classFQN) to invokable command action */
    protected string $commandInvokableClass = ExportTurnovers\ExportTurnovers::class;

    /** @var InputInterface */
    private $input;
    /** @var OutputInterface */
    private $output;

    function configure(): void
    {
        $this->setName('export-csv')
            ->setDescription(
                'Generate a CSV file include, Turnover per brand and Turnover per day (of all brands)' . PHP_EOL
                . "\t\t Default date range is the first 7 days of the month May (01-05-2018 - 07-05-2018).");
    }

    /**
     * @inheritDoc
     */
    function getExecutableArguments(): array
    {
        return [
            'input' => $this->input,
            'output' => $this->output,
        ];
    }

    /**
     * @inheritDoc
     */
    protected function beforeExecute(InputInterface $input, OutputInterface $output): void
    {
        $this->input = $input;
        $this->output = $output;
    }
}
