<?php
declare(strict_types=1);

namespace Otrium\Console\Commands\CreateTables;

use Otrium\Console\BaseCommand;
use Symfony\Component\Console\Output\OutputInterface;


class CreateTables
{
    /**
     * CreateTables
     *
     * @param \PDO $connection
     */
    function __construct(protected \PDO $connection)
    { }

    /**
     * Create database tables
     *
     * @param OutputInterface $output
     */
    function __invoke(OutputInterface $output): void
    {
        try {
            $this->createTables();
        } catch (\Exception $e) {
            $output->writeln(BaseCommand::MSG_ERROR_COLOR . $e->getMessage());
            $output->writeln(BaseCommand::MSG_RESET_COLOR);
            return;
        }

        $output->writeln(BaseCommand::MSG_INFO_COLOR . 'Tables created successfully.');
        $output->writeln(BaseCommand::MSG_RESET_COLOR);
    }

    // Create Tables:

    private function createTables(): void
    {
        $this->createBrandsTable();
        $this->createGmvTable();
    }

    private function createBrandsTable(): void
    {
        $sql = <<< SQL
        CREATE TABLE `brands` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `name` varchar(255) DEFAULT NULL,
          `description` varchar(255) DEFAULT NULL,
          `products` int(11) DEFAULT NULL,
          `created` datetime DEFAULT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        SQL;

        $this->connection->exec($sql);
    }

    private function createGmvTable(): void
    {
        $sql = <<< SQL
        CREATE TABLE `gmv` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `brand_id` int(11) unsigned NOT NULL,
          `date` datetime DEFAULT NULL,
          `turnover` decimal(10,2) NOT NULL DEFAULT '0.00',
          PRIMARY KEY (`id`),
          KEY `brand_id` (`brand_id`),
          CONSTRAINT `brand_gmv` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        SQL;

        $this->connection->exec($sql);
    }
}
