
LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;

INSERT INTO `brands` (`id`, `name`, `description`, `products`, `created`)
VALUES
	(1,'O-Brand',NULL,4,'2018-01-01 00:00:00'),
	(2,'T-Brand',NULL,2472,'2018-02-01 00:00:00'),
	(3,'R-Brand',NULL,2104,'2018-03-01 00:00:00'),
	(4,'I-Brand',NULL,1558,'2018-04-01 00:00:00'),
	(5,'U-Brand',NULL,2419,'2018-05-01 00:00:00'),
	(6,'M-Brand',NULL,362,'2018-06-01 00:00:00'),
	(7,'X-Brand',NULL,489,'2018-07-01 00:00:00'),
	(8,'Y-Brand',NULL,2412,'2018-08-01 00:00:00'),
	(9,'Z-Brand',NULL,1788,'2018-09-01 00:00:00'),
	(10,'Q-Brand',NULL,2223,'2018-10-01 00:00:00'),
	(11,'B-Brand',NULL,3309,'2018-11-01 00:00:00'),
	(12,'AA-Brand',NULL,3332,'2018-12-01 00:00:00');

/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;
