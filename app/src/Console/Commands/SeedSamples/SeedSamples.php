<?php
declare(strict_types=1);

namespace Otrium\Console\Commands\SeedSamples;

use Otrium\Console\BaseCommand;
use Symfony\Component\Console\Output\OutputInterface;


class SeedSamples
{
    const BrandsTable = 'brands';
    const GmvTable = 'gmv';

    /**
     * CreateTables
     *
     * @param \PDO $connection
     */
    function __construct(protected \PDO $connection)
    { }

    /**
     * Seed Database With Sample Data
     *
     * @param OutputInterface $output
     */
    function __invoke(OutputInterface $output): void
    {
        try {
            $this->importSeeds();
        } catch (\Exception $e) {
            $output->writeln(BaseCommand::MSG_ERROR_COLOR . $e->getMessage());
            $output->writeln(BaseCommand::MSG_RESET_COLOR);
            return;
        }

        $output->writeln(BaseCommand::MSG_INFO_COLOR . 'Tables sample data imported successfully.');
        $output->writeln(BaseCommand::MSG_RESET_COLOR);
    }

    // ..

    protected function importSeeds()
    {
        foreach ([self::BrandsTable, self::GmvTable] as $table) {
            $change = $this->connection->exec($this->readData($table));
            if (false === $change) {
                throw new \RuntimeException($this->connection->errorInfo());
            }
        }
    }

    private function readData(string $name): string
    {
        $name = strtolower($name);

        $filename = __DIR__ . "/files/$name.sql";
        if (false === $sqlContent = @file_get_contents($filename))
            throw new \InvalidArgumentException(sprintf(
                'Seeder file "%s" not readable or not found.'
                , $filename
            ));

        return $sqlContent;
    }
}
