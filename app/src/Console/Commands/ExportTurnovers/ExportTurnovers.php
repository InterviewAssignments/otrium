<?php
declare(strict_types=1);

namespace Otrium\Console\Commands\ExportTurnovers;

use Otrium\Console\BaseCommand;
use Otrium\Exceptions\DataProcessor\SyntaxError;
use Otrium\Interfaces\iTabularData;
use Otrium\Interfaces\iTabularDataWriter;
use Otrium\Interfaces\Repository\iTurnoversRepo;
use Otrium\Libraries\TabularData;
use Otrium\Models\Entities\Brands\Brand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class ExportTurnovers
{
    private const DateRange = [
        'from' => '01-05-2018',
        'to' => '07-05-2018',
        'format' => 'd-m-Y',
    ];

    /**
     * ExportCsv
     *
     * @param iTurnoversRepo $turnoversRepo
     * @param iTabularDataWriter $dataWriter
     */
    function __construct(
        private iTurnoversRepo $turnoversRepo,
        private iTabularDataWriter $dataWriter
    ) {

    }

    /**
     * Export Brand`s Turnover To CSV File
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    function __invoke(InputInterface $input, OutputInterface $output): void
    {
        $startDate = \DateTime::createFromFormat(self::DateRange['format'], self::DateRange['from']);
        $endDate   = \DateTime::createFromFormat(self::DateRange['format'], self::DateRange['to']);

        try {
            $turnoversResult = $this->turnoversRepo
                ->findTurnoversSegregatedByDateIn($startDate, $endDate, true);

            $this->dataWriter->writeOutput(
                $this->createTabularDatasetFromResult($turnoversResult)
            );

        } catch (\Throwable $error) {
            $output->writeln(BaseCommand::MSG_ERROR_COLOR . $error->getMessage());
            $output->writeln(BaseCommand::MSG_RESET_COLOR);
            return;
        }

        $output->writeln(BaseCommand::MSG_INFO_COLOR . "Turnovers file generated successfully.");
        $output->writeln(BaseCommand::MSG_RESET_COLOR);
    }

    /**
     * Dump Data To Output Resource
     *
     * @param \Iterator $turnoversResult
     *
     * @return iTabularData
     * @throws SyntaxError
     */
    private function createTabularDatasetFromResult(\Iterator $turnoversResult): iTabularData
    {
        foreach ($turnoversResult as $brandTurnover)
        {
            (! isset($tabularData)) && $tabularData = new TabularData([
                'id', 'name', ...array_keys($brandTurnover['turnovers'])
            ]);

            /** @var Brand $brand */
            $brand = $brandTurnover['brand'];
            $tabularData->appendNewRecord(
                [$brand->brandId, $brand->name, ...array_values($brandTurnover['turnovers'])]
            );
        }

        return $tabularData ?? new TabularData([]);
    }
}
