<?php
declare(strict_types=1);

namespace Otrium\Console\Commands;

use Otrium\Console\BaseCommand;
use Otrium\Console\Commands\SeedSamples\SeedSamples;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class SeedSamplesCommand
    extends BaseCommand
{
    /** @var string FQCN (classFQN) to invokable command action */
    protected string $commandInvokableClass = SeedSamples::class;

    /** @var OutputInterface */
    private $output;

    function configure(): void
    {
        $this->setName('seed-samples')
             ->setDescription('Seed database with sample data.');
    }

    /**
     * @inheritDoc
     */
    function getExecutableArguments(): array
    {
        return [
            'output' => $this->output,
        ];
    }

    /**
     * @inheritDoc
     */
    protected function beforeExecute(InputInterface $input, OutputInterface $output): void
    {
        $this->output = $output;
    }
}
