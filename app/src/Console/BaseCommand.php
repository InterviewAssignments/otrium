<?php
declare(strict_types=1);

namespace Otrium\Console;

use Poirot\ServiceManager\Interfaces\Pacts\iServicesProvider;
use Poirot\ServiceManager\Services\Instance\ArgumentsResolver\InstantiableServicesResolver;
use Poirot\Std\ArgumentsResolver;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Command Classes extending this can get the required parameters
 * injected with Auto-Wiring on Construct level.
 *
*/
class BaseCommand
    extends SymfonyCommand
{
    const MSG_INFO_COLOR = "\033[0;32m";
    const MSG_ERROR_COLOR = "\033[0;31m";
    const MSG_RESET_COLOR = "\033[0m";

    /** @var string FQCN (classFQN) to invokable command action */
    protected string $commandInvokableClass = self::class;

    /**
     * @inheritDoc
     */
    final protected function execute(InputInterface $input, OutputInterface $output)
    {
        $callableExecutable = $this->_resolveInvokableCommand(
            $this->commandInvokableClass,
            [
                'input' => $input,
                'output' => $output,
                'command' => $this,
            ]
        );

        $this->beforeExecute($input, $output);

        ($this->_resolveArguments($callableExecutable, $this->getExecutableArguments()))();

        return self::SUCCESS;
    }

    /**
     * Before Execute Command Executable
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function beforeExecute(InputInterface $input, OutputInterface $output): void
    {
        // Implement By Class Extending This
    }

    /**
     * Provide Arguments To Executable Callable
     *
     * @return array
     */
    function getExecutableArguments(): array
    {
        // Implement By Class Extending This
        return [];
    }

    /**
     * Set Executable Action
     *
     * @param string $executable FQCN of executable class
     *
     * @return $this
     */
    function setExecutable(string $executable): self
    {
        if (! class_exists($executable))
            throw new \InvalidArgumentException(sprintf(
                'Command should be a full qualified name to an existing class; given: "%s".'
                , $executable
            ));

        $this->commandInvokableClass = $executable;
        return $this;
    }

    /**
     * @inheritDoc
     * @return Application|null
     */
    function getApplication(): ?Application
    {
        /** @var Application $application */
        $application = parent::getApplication();
        if (! $application instanceof iServicesProvider)
            throw new \RuntimeException(
                'Application who run the lazy service aware command should be instance of iServicesProvider'
            );

        return $application;
    }

    // ..

    /**
     * Resolve And Inject Dependencies To Construct Level Of Command Executable
     *
     * @param string $executable
     * @param array $defaultOptions
     *
     * @return callable Invokable command
     */
    private function _resolveInvokableCommand(string $executable, array $defaultOptions): callable
    {
        $serviceManager = $this->getApplication()
            ->servicesContainer();

        return (new ArgumentsResolver(
            new InstantiableServicesResolver($executable, $serviceManager, $defaultOptions)
        ))->resolve();
    }

    /**
     * Resolve Dependencies To Invokable Method Of Executable Command In Method Arguments Level
     *
     * @param callable $invokableCommand
     * @param array $options
     *
     * @return \Closure Executable consist of resolved dependencies
     */
    private function _resolveArguments(callable $invokableCommand, array $options): \Closure
    {
        return (new ArgumentsResolver(
            new ArgumentsResolver\CallableResolver($invokableCommand, $options)
        ))->resolve();
    }
}
