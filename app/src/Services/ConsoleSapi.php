<?php
declare(strict_types=1);

namespace Otrium\Services;

use Otrium\Interfaces\iApplication;
use Poirot\ServiceManager\Container;
use Poirot\ServiceManager\Interfaces\iServicesContainer;
use Poirot\ServiceManager\Interfaces\Pacts\iServicesProvider;
use Symfony\Component\Console\Application as SymfonyApplication;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class ConsoleSapi
    extends SymfonyApplication
    implements iApplication
    , iServicesProvider
{
    protected const APP_NAME = 'Otrium Assignment';
    protected const APP_VER = '1.0.0';

    private Container $servicesContainer;
    private bool $isRunning = false;

    /**
     * SapiCli
     *
     * @param iServicesContainer|Container|null $servicesContainer
     */
    function __construct(?iServicesContainer $servicesContainer = null)
    {
        $servicesContainer !== null &&
            $this->servicesContainer = $servicesContainer;

        parent::__construct(self::APP_NAME, self::APP_VER);
    }

    /**
     * @inheritDoc
     */
    function run(InputInterface $input = null, OutputInterface $output = null): void
    {
        if ($this->isRunning) {
            throw new \RuntimeException('Application currently is running and cant be run again.');
        }

        parent::run($input, $output);
    }

    // Implement Service Provider Interface

    /**
     * @inheritDoc
     */
    function servicesContainer(): iServicesContainer
    {
        if (! $this->servicesContainer instanceof iServicesContainer)
            $this->servicesContainer = new Container;

        return $this->servicesContainer;
    }
}
