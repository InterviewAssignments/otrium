<?php
declare(strict_types=1);

namespace Otrium\Services\Console;

use Otrium\Services\ConsoleSapi;
use Poirot\ServiceManager\Container\Builder as ContainerBuilder;
use Poirot\Std\Configurable\aConfigurableSetter;


class Builder
    extends aConfigurableSetter
{
    protected $services;
    protected array $commands = [];
    protected string $defaultCommand;


    /**
     * Build Application With Given Parameters
     *
     * @param ConsoleSapi $application
     *
     * @return ConsoleSapi
     */
    function build(ConsoleSapi $application): ConsoleSapi
    {
        try {
            $this->buildServices($application);
            $this->buildCommands($application);
        } catch (\Exception $e) {
            throw new \RuntimeException(
                'Error while building Application with configuration parameters.',
                $e->getCode(),
                $e
            );
        }

        return $application;
    }

    // Setter Options: (called when derived config parameter given)

    /**
     * Set Application Commands
     *
     * 'commands' => [
     *    new Command(),
     * ],
     *
     * @param array $commands
     *
     * @return $this
     */
    protected function setCommands(array $commands): self
    {
        $this->commands = $commands;

        return $this;
    }

    /**
     * Set Default Command Name
     *
     * @param string $commandName
     *
     * @return $this
     */
    protected function setDefaultCommand(string $commandName): self
    {
        $this->defaultCommand = $commandName;

        return $this;
    }

    /**
     * Set Application Service Manager Setting
     *
     * @param array $services
     *
     * @return $this
     */
    protected function setServices(array $services): self
    {
        $this->services = $services;

        return $this;
    }

    // Build Application with given configurations:

    protected function buildServices(ConsoleSapi $application): void
    {
        if (empty($this->services))
            return;

        $containerBuilder = new ContainerBuilder(ContainerBuilder::parse($this->services));
        $containerBuilder->build(
            $application->servicesContainer()
        );
    }

    protected function buildCommands(ConsoleSapi $application): void
    {
        if (empty($this->commands))
            return;

        $application->addCommands($this->commands);

        if (! empty($this->defaultCommand)) {
            $application->setDefaultCommand($this->defaultCommand, true);
        }
    }
}
