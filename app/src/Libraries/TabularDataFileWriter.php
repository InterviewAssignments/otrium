<?php
declare(strict_types=1);

namespace Otrium\Libraries;

use Otrium\Exceptions\DataWriter\WriteError;
use Otrium\Interfaces\iTabularData;
use Otrium\Interfaces\iTabularDataWriter;


class TabularDataFileWriter
    implements iTabularDataWriter
{
    /** @var resource */
    private $hasOpenResource;

    /**
     * TabularDataFileWriter
     *
     * @param string $outputFilepath
     */
    function __construct(private string $outputFilepath)
    { }

    function __destruct()
    {
        $this->closeResource();
    }

    /**
     * @inheritDoc
     */
    function writeOutput(iTabularData $data, bool $includeHeaders = true): void
    {
        $fileResource = $this->createOutputResource();

        $tabularDataIterator = function() use ($data, $includeHeaders) {
            $includeHeaders && yield $data->getHeader();
            yield from $data->getRecords();
        };

        foreach ($tabularDataIterator() as $dataRow) {
            if (false === fputcsv($fileResource, $dataRow))
                throw WriteError::dueToErrorWhileTryingToWriteOutputDevice();
        }

        $this->closeResource();
    }

    // ..

    /**
     * Create Output Resource To Dump Result
     *
     * @return resource
     * @throws WriteError
     */
    private function createOutputResource()
    {
        if (false === $resource = fopen($this->outputFilepath, 'w')) {
            throw WriteError::dueToDeviceIsNotWritable();
        }

        return $this->hasOpenResource = $resource;
    }

    private function closeResource(): void
    {
        if (! isset($this->hasOpenResource)) {
            return;
        }

        fclose($this->hasOpenResource);
        unset($this->hasOpenResource);
    }
}
