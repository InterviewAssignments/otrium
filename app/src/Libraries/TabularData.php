<?php
declare(strict_types=1);

namespace Otrium\Libraries;

use Otrium\Exceptions\DataProcessor\SyntaxError;
use Otrium\Interfaces\iTabularData;


class TabularData
    implements iTabularData
{
    protected array $records = [];

    /**
     * TabularData
     *
     * @param array $header
     *
     * @throws SyntaxError
     */
    function __construct(protected array $header)
    {
        $this->assertValidateHeaders($header);
    }

    function __destruct()
    {
        unset($this->records);
    }

    /**
     * @inheritDoc
     */
    function appendNewRecord(array $cells): self
    {
        $this->records[] = $cells;

        return $this;
    }

    /**
     * @inheritDoc
     */
    function getHeader(): array
    {
        return $this->header;
    }

    /**
     * {@inheritdoc}
     */
    function getRecords(): \Iterator
    {
        foreach ($this->records as $offset => $value) {
            yield $offset => $value;
        }
    }

    // Implement Iterator:

    /**
     * @inheritDoc
     */
    function getIterator(): \Iterator
    {
        return $this->getRecords();
    }

    // Implement Countable:

    /**
     * @inheritDoc
     */
    function count(): int
    {
        return count($this->records);
    }

    // ..

    /**
     * Assert Validate Headers
     *
     * @param array $header
     *
     * @throws SyntaxError if the header syntax is invalid
     */
    protected function assertValidateHeaders(array $header): void
    {
        if ($header !== array_unique(array_filter($header, 'is_string'))) {
            throw SyntaxError::dueToInvalidHeaderContent();
        }
    }
}
