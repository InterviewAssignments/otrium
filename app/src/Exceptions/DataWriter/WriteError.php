<?php
namespace Otrium\Exceptions\DataWriter;


class WriteError
    extends Exception
{
    const CodeDeviceWrite = 00001;
    const CodeDeviceNotWritable = 00001;

    static function dueToErrorWhileTryingToWriteOutputDevice(): self
    {
        return new self(
            'Error While Trying To Output Device.'
            , self::CodeDeviceWrite
        );
    }

    static function dueToDeviceIsNotWritable(): self
    {
        return new self(
            'Device Is Not Writable Or Not Present.'
            , self::CodeDeviceNotWritable
        );
    }
}
