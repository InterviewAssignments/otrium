<?php
namespace Otrium\Exceptions\DataWriter;

use Exception as PhpException;
use Otrium\Interfaces\Exception\DataWriteError;


class Exception
    extends PhpException
    implements DataWriteError
{ }
