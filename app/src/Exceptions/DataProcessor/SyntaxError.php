<?php
namespace Otrium\Exceptions\DataProcessor;


/**
 * When there is syntax error in data rows
 */
class SyntaxError
    extends Exception
{
    const CodeHeaderContent = 00001;

    static function dueToInvalidHeaderContent(): self
    {
        return new self(
            'The header record must be an empty or a flat array with unique string values.'
            , self::CodeHeaderContent
        );
    }
}
