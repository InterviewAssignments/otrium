<?php
namespace Otrium\Exceptions\DataProcessor;

use Exception as PhpException;
use Otrium\Interfaces\Exception\UnableToProcessDataError;


class Exception
    extends PhpException
    implements UnableToProcessDataError
{ }
