<?php
namespace Otrium\Interfaces\Entity;

use Otrium\Models\Entities\Brands\Brand;


interface iBrandFactory
{
    /**
     * Create Empty Entity
     *
     * @return Brand
     */
    function create(): Brand;

    /**
     * Create Entity From Given Parameters
     *
     * @param array $params
     *
     * @return Brand
     * @throws \InvalidArgumentException Missing or invalid params
     */
    function createFromParams(array $params): Brand;
}
