<?php
namespace Otrium\Interfaces;

use Otrium\Interfaces\Exception\DataWriteError;


interface iTabularDataWriter
{
    /**
     * Write Tabular Data To Output Medium
     *
     * @param iTabularData $data
     * @param bool $includeHeaders
     *
     * @throws DataWriteError
     */
    function writeOutput(iTabularData $data, bool $includeHeaders = true): void;
}
