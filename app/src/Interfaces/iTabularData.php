<?php
namespace Otrium\Interfaces;


interface iTabularData
    extends \Countable, \IteratorAggregate
{
    /**
     * Append New Record
     *
     * @param array $cells
     *
     * @return $this
     */
    function appendNewRecord(array $cells): self;

    /**
     * Returns the header associated with the data
     *
     * @return string[]
     */
    function getHeader(): array;

    /**
     * Returns the tabular data records as an iterator object.
     *
     * Each record is represented as a simple array containing strings or null values.
     *
     * @return \Iterator
     */
    function getRecords(): \Iterator;

    /**
     * Returns the number of records contained in the data structure
     * excluding the header record.
     *
     * @return int
     */
    function count(): int;

    /**
     * Returns the tabular data records as an iterator object.
     * Each record is represented as a simple array containing
     * strings or null values.
     *
     * @return \Iterator
     */
    function getIterator(): \Iterator;
}
