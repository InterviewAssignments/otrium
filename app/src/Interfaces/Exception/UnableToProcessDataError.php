<?php
namespace Otrium\Interfaces\Exception;

/**
 * When Tabular Data Is Not Processable Due To Structure Error
 */
interface UnableToProcessDataError
    extends \Throwable
{ }
