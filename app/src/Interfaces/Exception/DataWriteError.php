<?php
namespace Otrium\Interfaces\Exception;

/**
 * When Unable To Dump Data To Medium Destination
 */
interface DataWriteError
    extends \Throwable
{ }
