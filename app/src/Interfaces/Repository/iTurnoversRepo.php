<?php
namespace Otrium\Interfaces\Repository;


interface iTurnoversRepo
{
    /**
     * Get All Brands Turnover per day
     *
     * @param \DateTime      $startDate
     * @param \DateTime|null $endDate Current date when it's null
     * @param bool           $includeTotal Include turnover sum in given date range
     *
     * @return \Iterator<array>
     */
    function findTurnoversSegregatedByDateIn(\DateTime $startDate, ?\DateTime $endDate, $includeTotal = false): \Iterator;
}
