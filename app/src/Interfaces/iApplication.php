<?php
namespace Otrium\Interfaces;


interface iApplication 
{
    /**
     * Run Application
     *
     * @return void
     */
    function run(): void;
}
