<?php
declare(strict_types=1);

namespace Otrium\Models\Entities\Brands;

use JetBrains\PhpStorm\Pure;
use Otrium\Interfaces\Entity\iBrandFactory;


class BrandFactory
    implements iBrandFactory
{
    /**
     * @inheritDoc
     */
    #[Pure] function create(): Brand
    {
        return new Brand();
    }

    /**
     * @inheritDoc
     */
    function createFromParams(array $params): Brand
    {
        $this->assertValidateParams($params);

        $brand = $this->create();
        $brand->brandId = (int) $params['id'];
        $brand->name = (string) $params['name'];

        return $brand;
    }

    // ..

    /**
     * Validate given parameters against required order params.
     *
     * @param array $params
     *
     * @throws \InvalidArgumentException
     */
    private function assertValidateParams(array $params): void
    {
        $requiredParams = ['id', 'name'];

        if ($requiredParams !== array_intersect($requiredParams, array_keys($params))) {
            // TODO exception message includes the missing parameter(s).
            throw new \InvalidArgumentException('Missing Required Parameters.');
        }
    }
}
