<?php
declare(strict_types=1);

namespace Otrium\Models\Entities\Brands;


class Brand
{
    public ?int $brandId;
    public string $name;
}
