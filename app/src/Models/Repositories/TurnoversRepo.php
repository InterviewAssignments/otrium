<?php
declare(strict_types=1);

namespace Otrium\Models\Repositories;

use Otrium\Interfaces\Entity\iBrandFactory;
use Otrium\Interfaces\Repository\iTurnoversRepo;
use Poirot\Std\IteratorWrapper;
use Poirot\Std\Type\StdArray;


class TurnoversRepo
    implements iTurnoversRepo
{
    protected $tableBrands = 'brands';
    protected $tableGmv = 'gmv';

    /**
     * UsersRepo
     *
     * @param \PDO $dbConnection
     * @param iBrandFactory $brandFactory
     */
    function __construct(protected \PDO $dbConnection, protected iBrandFactory $brandFactory)
    { }

    /**
     * @inheritDoc
     */
    function findTurnoversSegregatedByDateIn(\DateTime $startDate, ?\DateTime $endDate, $includeTotal = false): \Iterator
    {
        $stmt = $this->dbConnection->query("
        SELECT
            brands.id,
            brands.name,   
            {$this->getSubQueryToDateSegregation()},
            SUM(gmv.turnover) total_in_range
        FROM {$this->tableGmv} as gmv
        JOIN {$this->tableBrands} as brands ON brands.id = gmv.brand_id
        WHERE gmv.date between '2018-05-01' and '2018-05-07'
        GROUP BY brand_id
        ");

        $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        // Change items stream to domain entity object
        return new IteratorWrapper($results, function ($row, $index, $self) {
            return [
                'brand' => $this->brandFactory->createFromParams([
                    'id' => $row['id'],
                    'name' => $row['name'],
                ]),
                'turnovers' => StdArray::of($row)->except('id', 'name')->asNativeArray,
            ];
        });
    }

    private function getSubQueryToDateSegregation(): string
    {
        $stmt = $this->dbConnection->query("
        SELECT
          GROUP_CONCAT(DISTINCT
            CONCAT( 'SUM(IF(date_format(gmv.date, \'%m-%d\') = ''', date_format(gmv.date, '%m-%d'), ''', gmv.turnover,0)) as `', 
                date_format(gmv.date, '%Y-%m-%d'), '`'
            )
          ) as date_range_query
        from gmv
        where gmv.date between '2018-05-01' and '2018-05-07';
        ");

        $selectDateRanges = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $selectDateRanges['date_range_query'];
    }
}
