<?php
declare(strict_types=1);

use Otrium\Interfaces\iApplication;
use Poirot\ServiceManager\Container;

// Anonymous self called function to Keep global namespace clean
(function () {
    !defined('DS') and define('DS', DIRECTORY_SEPARATOR);
    !defined('DIR_ROOT') and define('DIR_ROOT', __DIR__);

    define('DIR_CONFIG', DIR_ROOT . DS . 'conf');
    define('DIR_RUNTIME', DIR_ROOT . DS . 'runtime');

    // change cwd to the application root by default
    chdir(DIR_ROOT);

    ## Initialize Application And Register Autoload
    #
    (! defined('PHP_VERSION_ID') or PHP_VERSION_ID < 80001)
    && exit('Needs at least PHP8.0; your current php version is ' . phpversion() . '.');

    if (! file_exists(DIR_ROOT . '/vendor/autoload.php'))
        throw new \RuntimeException("Unable to run Application.\n"
            . "- Type `composer install`; to install library dependencies.\n"
        );

    require DIR_ROOT . '/vendor/autoload.php';

    $IoC = new Container(new Container\Builder(
        Container\Builder::from(DIR_CONFIG . '/services.php')
    ));

    $app = $IoC->get(iApplication::class);
    $app->run();
})();
