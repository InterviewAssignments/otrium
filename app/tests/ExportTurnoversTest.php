<?php
namespace OtriumTest;

use Otrium\Console\Commands\ExportTurnovers\ExportTurnovers;
use Otrium\Interfaces\iTabularData;
use Otrium\Interfaces\iTabularDataWriter;
use Otrium\Interfaces\Repository\iTurnoversRepo;
use Otrium\Models\Entities\Brands\Brand;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class ExportTurnoversTest
    extends TestCase
{
    function test_is_invokable()
    {
        $this->assertIsCallable(
            new ExportTurnovers(
                $this->createMockedTurnoverRepo(),
                $this->createMockedDataWriter()
            )
        );
    }

    function test_repository_exception_will_be_handled()
    {
        $errorMessage = 'error message';

        $exportTurnovers = new ExportTurnovers(
            $this->createMockedTurnoverRepo(function ($repo) use ($errorMessage) {
                $repo->expects($this->once())
                    ->method('findTurnoversSegregatedByDateIn')
                    ->willThrowException(new \Exception($errorMessage));
            }),
            $this->createMockedDataWriter()
        );

        $exportTurnovers(
            $this->createMockedInput(),
            $this->createMockedOutput(function ($output) use ($errorMessage) {
                // error should be logged to output
                $output->expects($this->any())
                    ->method('writeln')
                    ->withConsecutive(
                        [$this->stringContains($errorMessage), 0],
                        [$this->isType('string'), 0],
                    );
            })
        );
    }

    function test_data_writer_exception_will_be_handled()
    {
        $errorMessage = 'error message';

        $exportTurnovers = new ExportTurnovers(
            $this->createMockedTurnoverRepo(),
            $this->createMockedDataWriter(function ($repo) use ($errorMessage) {
                $repo->expects($this->once())
                    ->method('writeOutput')
                    ->willThrowException(new \Exception($errorMessage));
            })
        );

        $exportTurnovers(
            $this->createMockedInput(),
            $this->createMockedOutput(function ($output) use ($errorMessage) {
                // error should be logged to output
                $output->expects($this->any())
                    ->method('writeln')
                    ->withConsecutive(
                        [$this->stringContains($errorMessage), 0],
                        [$this->isType('string'), 0],
                    );
            })
        );
    }

    function test_can_create_tabular_data_set_to_be_write()
    {
        $exportTurnovers = new ExportTurnovers(
            $this->createMockedTurnoverRepo(function ($repo) {
                $repo->expects($this->once())
                    ->method('findTurnoversSegregatedByDateIn')
                    ->willReturn($this->getDummyTurnoverData());
            }),
            $this->createMockedDataWriter(function ($repo) {
                $repo->expects($this->once())
                    ->method('writeOutput')
                    ->with($this->isInstanceOf(iTabularData::class));
            })
        );

        $exportTurnovers(
            $this->createMockedInput(),
            $this->createMockedOutput(function ($output) {
                $output->expects($this->any())
                    ->method('writeln');
            })
        );
    }

    function test_tabular_data_validity()
    {
        $methodReflection = $this->makeMethodAccessible(
            ExportTurnovers::class,
            'createTabularDatasetFromResult'
        );

        /** @var iTabularData $tabularData */
        $tabularData = $methodReflection->invoke(
            new ExportTurnovers(
                $this->createMockedTurnoverRepo(),
                $this->createMockedDataWriter()
            ),
            $this->getDummyTurnoverData()
        );

        $this->assertInstanceOf(iTabularData::class, $tabularData);

        // Test header columns
        $this->assertEquals(['id', 'name', '2018-05-01', '2018-05-02', 'total_in_range'], $tabularData->getHeader());

        // Test data created by turnover exporter are expected
        $turnOversRawData = $this->getDummyTurnoverData();
        $turnOversRawData = iterator_to_array($turnOversRawData);
        foreach($tabularData->getRecords() as $offset => $row) {
            $expectedRowData = $turnOversRawData[$offset];
            $this->assertEquals(
                [$expectedRowData['brand']->brandId, $expectedRowData['brand']->name, ...array_values($expectedRowData['turnovers'])],
                $row
            );
        }
    }

    // Helpers:

    function makeMethodAccessible($class, $methodName): \ReflectionMethod
    {
        $class = new \ReflectionClass($class);
        $method = $class->getMethod($methodName);
        $method->setAccessible(true);

        return $method;
    }

    protected function createMockedTurnoverRepo(?\Closure $wrapper = null): iTurnoversRepo
    {
        $mocked = $this->getMockBuilder(iTurnoversRepo::class)->getMock();
        $wrapper !== null && $wrapper($mocked);

        return $mocked;
    }

    protected function createMockedDataWriter(?\Closure $wrapper = null): iTabularDataWriter
    {
        $mocked = $this->getMockBuilder(iTabularDataWriter::class)->getMock();
        $wrapper !== null && $wrapper($mocked);

        return $mocked;
    }

    protected function createMockedInput(?\Closure $wrapper = null): InputInterface
    {
        $mocked = $this->getMockBuilder(InputInterface::class)->getMock();
        $wrapper !== null && $wrapper($mocked);

        return $mocked;
    }

    protected function createMockedOutput(?\Closure $wrapper = null): OutputInterface
    {
        $mocked = $this->getMockBuilder(OutputInterface::class)->getMock();
        $wrapper !== null && $wrapper($mocked);

        return $mocked;
    }

    protected function createMockedBrand(mixed $id, mixed $name): Brand
    {
        $mocked = $this->getMockBuilder(Brand::class)->getMock();
        $mocked->brandId = $id;
        $mocked->name = $name;

        return $mocked;
    }

    protected function getDummyTurnoverData()
    {
        return (function () {
            foreach (include __DIR__ . '/ExportTurnovers/dumyData.php' as $row) {
                $id = $row['id'];
                $name = $row['name'];

                unset($row['id']);
                unset($row['name']);

                yield [
                    'brand' => $this->createMockedBrand($id, $name),
                    'turnovers' => $row,
                ];
            }
        })();
    }
}
